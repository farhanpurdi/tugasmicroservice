package com.training.app.micro.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.app.micro.model.Nasabah;
import com.training.app.micro.model.Rekening;
import com.training.app.micro.repository.NasabahRepository;
import com.training.app.micro.repository.RekeningRepository;

@Service("nasabahService")
@CacheConfig(cacheNames = "nasabahService")
public class NasabahService {

	
	@Autowired
	private NasabahRepository nasabahRepository;
	
	
	@Autowired
	private RekeningRepository rekeningRepository;
	
	
//	private Log LOGGER = LogFactory.getLog(NasabahRepository.class);
	
	@Cacheable(value = "nasavah.user.findall", unless = "#result == null")
	public List<Nasabah> findAll() {
		return nasabahRepository.findAll();
	}
	
	@Cacheable(value = "nasabah.user.findById", unless = "#result == null")
	public Nasabah findById(Long id) {
		return nasabahRepository.findById(id).get();
	}

	
	public Nasabah save(Nasabah nasabah) {
		return nasabahRepository.save(nasabah);
	}
	
	public Rekening saverek(Rekening rekening) {
		return rekeningRepository.save(rekening);
	}
	
	@Cacheable(value = "nasabah.user.findByNoRek", unless = "#result == null")
	public Rekening findByNoRekLike(String account) {
		return rekeningRepository.findByNoRekLike(account);
	}
	
	
	public void newNasabah(Nasabah nasabah) {
		nasabahRepository.saveAndFlush(nasabah);
	}
	
	@Cacheable(value = "nasabah.user.findByNama", unless = "#result == null")
	public Nasabah findByNama(String nama) {
		return nasabahRepository.findByNamaLike(nama);
	}
	
	// Transaksional untuk mencegah terjadinya transaksi apabila ada error yang ditemukan pada sebuah method
	// untuk memproses dan mencegah terjadinya error pada banyak proses yang dilakukan di dalam satu method
	
	@Transactional
	public void cobaTransactional() {
//		
//		// Mengubah alamat dengan id 1231
//		Employee employee = employeeRepository.findById(1231L).get();
//
//		LOGGER.debug("Alamat sebelum : " + employee.getAddress());
//
//		employee.setAddress("BSD City");
//		employeeRepository.save(employee);
//
//		LOGGER.debug("Alamat setelah : " + employee.getAddress());
//		
//		// Perubahan task yang dilakukan pada id 1231 dengan task id = 1
//
//		Task task = taskRepository.findById(1L).get();
//
//		LOGGER.debug("Nama sebelum : " + task.getNama());
//
//		task.setNama("Bukan Ngoding Lah");
//		taskRepository.save(task);
//		LOGGER.debug("Nama setelah : " + task.getNama());
	}
}
