package com.training.app.micro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.app.micro.model.Nasabah;

public interface NasabahRepository extends JpaRepository<Nasabah, Long>{
	
	Nasabah findByNamaLike(String nama);
}
