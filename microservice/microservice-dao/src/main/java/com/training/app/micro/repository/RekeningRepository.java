package com.training.app.micro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.app.micro.model.Rekening;

public interface RekeningRepository extends JpaRepository<Rekening, Integer>{
	
	Rekening findByNoRekLike(String account);
}
