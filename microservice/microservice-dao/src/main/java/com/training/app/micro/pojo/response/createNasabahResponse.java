package com.training.app.micro.pojo.response;

public class createNasabahResponse {
	public String success( ) {
		return "Data Nasabah berhasil diinput";
	}
	
	public String failed( ) {
		return "Data Nasabah gagal diinput";
	}
	
	public String error( ) {
		return "Data Nasabah mengalami error input";
	}
}
