package com.training.app.micro.pojo.response;

public class updateDataNasabahResponse {
	public String success() {
		return "Data berhasil di update";
	}
	
	public String failed() {
		return "Data tidak ditemukan";
	}
	
	public String error() {
		return "Data mengalami error";
	}
}
