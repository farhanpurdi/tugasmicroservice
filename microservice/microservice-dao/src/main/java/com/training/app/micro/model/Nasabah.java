package com.training.app.micro.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="nasabah")
public class Nasabah {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false, length=10)
	private Long id;
	
	@Column(name="namaNasabah", nullable=false )
	private String nama;
	
	@Column(name="phone", nullable=true)
	private String phone;
	
	@Column(name="address", nullable=true)
	private String address;

	public Long getId() {
		return id;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="nasabah")
	private Set<Rekening> rek;

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Set<Rekening> getRek() {
		return rek;
	}

	public void setRek(Set<Rekening> rek) {
		this.rek = rek;
	}

	@Override
	public String toString() {
		return "Nasabah [id=" + id + ", nama=" + nama + ", phone=" + phone + ", address=" + address + ", rek=" + rek
				+ "]";
	}
}
