package com.training.app.micro.pojo.request;

public class transferRequest {
	private String rekeningAsal;
	private String rekeningTujuan;
	private String amount;
	
	
	public String getRekeningAsal() {
		return rekeningAsal;
	}
	public void setRekeningAsal(String rekeningAsal) {
		this.rekeningAsal = rekeningAsal;
	}
	public String getRekeningTujuan() {
		return rekeningTujuan;
	}
	public void setRekeningTujuan(String rekeningTujuan) {
		this.rekeningTujuan = rekeningTujuan;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
