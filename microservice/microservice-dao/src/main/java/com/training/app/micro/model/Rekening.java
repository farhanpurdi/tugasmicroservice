package com.training.app.micro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="rekening")
public class Rekening {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idrek", nullable=false)
	private int id;
	
	@Column(name="noRek", nullable=false, length=10)
	private String noRek;
	
	
	@Column(name="saldo", nullable=true)
	private String saldo;
	
	@JsonIgnore
	@OneToOne (fetch=FetchType.EAGER)
	@JoinColumn(name="nasabahId")
	private Nasabah nasabah;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}

	@Override
	public String toString() {
		return "Rekening [id=" + id + ", noRek=" + noRek + ", saldo=" + saldo + "]";
	}
}
