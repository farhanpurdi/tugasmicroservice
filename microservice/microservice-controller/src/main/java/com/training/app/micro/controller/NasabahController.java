package com.training.app.micro.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.training.app.micro.model.Nasabah;
import com.training.app.micro.model.Rekening;
import com.training.app.micro.pojo.request.cekSaldoRequest;
import com.training.app.micro.pojo.request.createNasabahRequest;
import com.training.app.micro.pojo.request.setorTunaiRequest;
import com.training.app.micro.pojo.request.tarikTunaiRequest;
import com.training.app.micro.pojo.request.transferRequest;
import com.training.app.micro.pojo.request.updateDataNasabahRequest;
import com.training.app.micro.pojo.response.cekSaldoResponse;
import com.training.app.micro.pojo.response.createNasabahResponse;
import com.training.app.micro.pojo.response.setorTunaiResponse;
import com.training.app.micro.pojo.response.tarikTunaiResponse;
import com.training.app.micro.pojo.response.transferResponse;
import com.training.app.micro.pojo.response.updateDataNasabahResponse;
import com.training.app.micro.service.NasabahService;

@RestController
public class NasabahController {

	private Log LOGGER = LogFactory.getLog(NasabahController.class);

	@Autowired
	private NasabahService nasabahService;

	private List<Nasabah> nasabahList;

	// FIND ALL DATA NASABAH
	@RequestMapping(path = "/minicore/account/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Nasabah> findAll() {
		nasabahList = nasabahService.findAll();
		for (Nasabah nasabah : nasabahList) {
			LOGGER.debug("Fecthcing Data : " + nasabah.getId() + " nama : " + nasabah.getNama() + " Alamat : "
					+ nasabah.getAddress() + " No. HP : " + nasabah.getPhone() + " Rekening : " + nasabah.getRek());
		}
		return nasabahList;
	}
	
	// CHECK ACCOUNT BALANCE
	@RequestMapping(path = "/minicore/account/balance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String cekSaldo(@RequestBody cekSaldoRequest request) {

		Rekening rekening = nasabahService.findByNoRekLike(request.getAccount());
		cekSaldoResponse response = new cekSaldoResponse();

		if (rekening == null) {
			return "AKUN TIDAK DITEMUKAN";
		} else {
			response.setBalance(rekening.getSaldo());
			return response.getBalance();
		}
	}

	// INQUIRY DATA NASABAH YANG DI PILIH
	@RequestMapping(path = "/minicore/account/inquiry/{id}/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public Nasabah listNasabahRekening(@PathVariable(name = "id") Long id) {
		Nasabah nasabah = new Nasabah();
		nasabah = nasabahService.findById(id);
		LOGGER.debug("Fecthcing Data : " + nasabah.getId() + " nama : " + nasabah.getNama() + " Alamat : "
				+ nasabah.getAddress() + " No. HP : " + nasabah.getPhone() + " Rekening : " + nasabah.getRek());
		return nasabah;
	}
	
	// ADD NASABAH
	@RequestMapping(path = "/minicore/account/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createNasabah(@RequestBody createNasabahRequest request) {
		createNasabahResponse response = new createNasabahResponse();
		Nasabah temp = new Nasabah();

		temp.setNama(request.getName());
		temp.setPhone(request.getPhone());
		temp.setAddress(request.getAddress());
	
		nasabahService.newNasabah(temp);
		return response.success();
	}
	
	// UPDATE DATA NASABAH
	@RequestMapping(path = "/minicore/account/update/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String save(@RequestBody updateDataNasabahRequest request) {
		updateDataNasabahResponse respone = new updateDataNasabahResponse();
		// Nasabah temp = null;
		try {
			Nasabah temp = nasabahService.findById(request.getId());
			LOGGER.debug("Object Found " + temp);
			LOGGER.debug("Update to " + request);
			// temp.setNama(request.getNama());
			temp.setAddress(request.getAddress());
			temp.setPhone(request.getPhone());
			nasabahService.save(temp);
			// service.save(temp);
			return respone.success();
		} catch (NoSuchElementException e) {
			return respone.failed();
		} catch (Exception e) {
			e.printStackTrace();
			return respone.error();
		}
	}
	
	

	// TARIK TUNAI
	@RequestMapping(path = "/minicore/transaction/withdrawal", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String withdrawal(@RequestBody tarikTunaiRequest request) {
		tarikTunaiResponse response = new tarikTunaiResponse();
		Rekening temp = nasabahService.findByNoRekLike(request.getNoRek());
		try {
			BigDecimal currentBalance = new BigDecimal(temp.getSaldo());

			LOGGER.debug("Saldo Sebelum Di Tambah : " + currentBalance);

			BigDecimal amount = new BigDecimal(request.getAmount());
			BigDecimal UpdateBalance = currentBalance.subtract(amount);

			LOGGER.debug("Saldo Sesudah Di Tambah : " + UpdateBalance);

			temp.setSaldo(UpdateBalance.toString());
			response.setNoRek(temp.getNoRek());
			response.setBalance(UpdateBalance.toString());
			nasabahService.saverek(temp);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "Nomor Rekening = " + response.getNoRek() + " Saldo = " + response.getBalance();
	}

	// SETOR TUNAI
	@RequestMapping(path = "/minicore/transaction/deposit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String deposit(@RequestBody setorTunaiRequest request) {
		setorTunaiResponse response = new setorTunaiResponse();
		Rekening temp = nasabahService.findByNoRekLike(request.getNoRek());
		try {
			BigDecimal currentBalance = new BigDecimal(temp.getSaldo());

			LOGGER.debug("Saldo Sebelum Di Tambah : " + currentBalance);

			BigDecimal amount = new BigDecimal(request.getAmount());
			BigDecimal UpdateBalance = currentBalance.add(amount);

			LOGGER.debug("Saldo Sesudah Di Tambah : " + UpdateBalance);

			temp.setSaldo(UpdateBalance.toString());
			response.setNoRek(temp.getNoRek());
			response.setBalance(UpdateBalance.toString());
			nasabahService.saverek(temp);
		} catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "Nomor Rekening = " + response.getNoRek() + " Saldo = " + response.getBalance();
	}
	
	@Transactional
	@RequestMapping(path = "/minicore/transaction/transfer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String transfer(@RequestBody transferRequest request) {
		transferResponse response = new transferResponse();

		try {
			Rekening temp1 = nasabahService.findByNoRekLike(request.getRekeningAsal());
			Rekening temp2 = nasabahService.findByNoRekLike(request.getRekeningTujuan());

			BigDecimal infoSaldo1 = new BigDecimal(temp1.getSaldo());
			BigDecimal infoSaldo2 = new BigDecimal(temp2.getSaldo());
			BigDecimal besarTransfer = new BigDecimal(request.getAmount());

			LOGGER.debug("Besar Saldo rekening 1 : " + infoSaldo1);
			LOGGER.debug("Besar saldo rekening 2 : " + infoSaldo2);
			if (infoSaldo1 != null) {

				BigDecimal potonganTransfer = infoSaldo1.subtract(besarTransfer);
				BigDecimal tranfer = infoSaldo2.add(besarTransfer);

				LOGGER.debug("besar setelah transfer : " + potonganTransfer);
				LOGGER.debug("Saldo setelah Transfer : " + infoSaldo1);
				LOGGER.debug("Saldo setelah Transfer : " + tranfer);

				temp1.setSaldo(potonganTransfer.toString());
				temp2.setSaldo(tranfer.toString());
				nasabahService.saverek(temp1);
				nasabahService.saverek(temp2);
				response.succes();
			}
			response.failed();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.error();
		}

		return response.succes();
	}
}
