package com.training.app.micro.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.app.micro.pojo.request.EchoRequest;
import com.training.app.micro.pojo.response.BaseResponse;


@RestController
public class EchoController {

	public static final String TERMINAL_ECHO_PATH = "nasabah/echo**";

	@RequestMapping(value = TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody
    BaseResponse echo(@RequestBody EchoRequest request) throws IOException {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("ECHO")) {
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
		} else {
			response.setResponseCode("01");
			response.setResponseMessage("FAILED");
		}
		return response;
	}
}