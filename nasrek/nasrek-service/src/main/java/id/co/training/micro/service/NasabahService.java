package id.co.training.micro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.training.micro.dao.feign.NasabahInterface;
import id.co.training.micro.dao.pojo.request.CekSaldoRequest;
import id.co.training.micro.dao.pojo.request.setorTunaiRequest;
import id.co.training.micro.dao.pojo.request.tarikTunaiRequest;

@Service("nasabahService")
public class NasabahService {

    @Autowired
    private NasabahInterface nasabahInterface;

//    public String userCheck(String username, String password) {
//        return thanosInterface.login(username,password);
//    }

    public String accBalance(CekSaldoRequest request) {
    	return nasabahInterface.cekSaldo(request);
    }
    
    public String accDepo(setorTunaiRequest request) {
    	return nasabahInterface.deposit(request);
    }
    
    public String accKre(tarikTunaiRequest request) {
    	return nasabahInterface.withdrawal(request);
    }
}
