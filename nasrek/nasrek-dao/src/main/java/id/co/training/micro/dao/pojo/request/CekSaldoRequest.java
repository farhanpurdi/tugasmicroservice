package id.co.training.micro.dao.pojo.request;

public class CekSaldoRequest {
	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}	
}