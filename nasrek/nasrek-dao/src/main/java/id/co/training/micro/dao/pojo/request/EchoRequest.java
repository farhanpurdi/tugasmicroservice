package id.co.training.micro.dao.pojo.request;

public class EchoRequest {
    private String requestCode;

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }
}
