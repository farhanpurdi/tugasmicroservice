package id.co.training.micro.dao.pojo.response;

public class tarikTunaiResponse {
	private String noRek;
	
	private String balance;

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "tarikTunaiResponse [noRek=" + noRek + ", balance=" + balance + "]";
	}
}
