package id.co.training.micro.dao.pojo.request;

public class setorTunaiRequest {

	private String noRek;
	
	private String addAmount;

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getAmount() {
		return addAmount;
	}

	public void setAmount(String addAmount) {
		this.addAmount = addAmount;
	}

	@Override
	public String toString() {
		return "setorTunaiRequest [noRek=" + noRek + ", addAmount=" + addAmount + "]";
	}
}
