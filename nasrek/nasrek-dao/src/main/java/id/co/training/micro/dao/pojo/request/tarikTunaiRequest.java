package id.co.training.micro.dao.pojo.request;

public class tarikTunaiRequest {
	private String noRek;

	private String reduceAmount;

	public String getNoRek() {
		return noRek;
	}

	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}

	public String getAmount() {
		return reduceAmount;
	}

	public void setAmount(String amount) {
		this.reduceAmount = amount;
	}

	@Override
	public String toString() {
		return "tarikTunaiRequest [noRek=" + noRek + ", amount=" + reduceAmount + "]";
	}
}
