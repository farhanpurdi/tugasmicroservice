package id.co.training.micro.dao.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.training.micro.dao.pojo.request.CekSaldoRequest;
import id.co.training.micro.dao.pojo.request.setorTunaiRequest;
import id.co.training.micro.dao.pojo.request.tarikTunaiRequest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient("nasabah")
public interface NasabahInterface {

    @HystrixCommand
    @HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
//    @RequestMapping(value = "/internal/userCheck", method = {RequestMethod.POST})
//    String login(@RequestParam(name = "username", required = false) String username, @RequestParam(name = "password", required = false) String password);
//
//    @RequestMapping(value = "/oauth/token",method = {RequestMethod.POST})
//    AuthTokenInfo loginOauth(@PathVariable("grant_type") String grantType, @PathVariable("username")String username,
//                             @PathVariable("password") String password);
    
    @RequestMapping(path = "/minicore/account/balance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	String cekSaldo(@RequestBody CekSaldoRequest request);
    
    @RequestMapping(path = "/minicore/transaction/withdrawal", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	String withdrawal(@RequestBody tarikTunaiRequest request);
    
    @RequestMapping(path = "/minicore/transaction/deposit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String deposit(@RequestBody setorTunaiRequest request);
}
