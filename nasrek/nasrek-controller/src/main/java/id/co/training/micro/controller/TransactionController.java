package id.co.training.micro.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.training.micro.dao.pojo.request.CekSaldoRequest;
import id.co.training.micro.dao.pojo.request.setorTunaiRequest;
import id.co.training.micro.dao.pojo.request.tarikTunaiRequest;
import id.co.training.micro.service.NasabahService;

@Controller("transactionController")
public class TransactionController {

    @Autowired
    private NasabahService nasabahService;
    
    private Log LOGGER = LogFactory.getLog(TransactionController.class);

    //public static final String AUTH_SERVER_URI = "http://localhost:8080/oauth/token";

    @RequestMapping(path = "/account/balance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody 
    Object cekSaldo(@RequestBody CekSaldoRequest request){
    	return nasabahService.accBalance(request);
    }
    
    @RequestMapping(path = "/transaction/deposit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody 
    Object deposit(@RequestBody setorTunaiRequest request){
    	return nasabahService.accDepo(request);
    }
    
    @RequestMapping(path = "/transaction/kredit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody 
    Object kredit(@RequestBody tarikTunaiRequest request){
    	return nasabahService.accKre(request);
    }
}